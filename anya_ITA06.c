#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <fuse.h>
#include <dirent.h>

static const char *dirpath = "/home/richard/soal-shift-sisop-modul-4-ita06-2022";
char prefix[10] = "Animeku_";

void enkripsi1(char *string)
{
  if (strcmp(string, ".") == 0 || strcmp(string, "..") == 0)
    return;

  char a[10];
  char new[1000];
  int totalN = 0, flag = 0, ext = 0;
  memset(a, 0, sizeof(a));
  memset(new, 0, sizeof(new));

  for (int i = 0; i < strlen(string); i++)
  {
    if (string[i] == '/')
      continue;
    if (string[i] == '.')
    {
      new[totalN++] = string[i];
      flag = 1;
    }
    if (flag == 1)
      a[ext++] = string[i];
    else
      new[totalN++] = string[i];
  }

  for (int i = 0; i < totalN; i++)
  {
    if (isupper(new[i]))
      new[i] = 'A' + 'Z' - new[i];
    else if (islower(new[i]))
    {
      if (new[i] > 109)
        new[i] -= 13;
      else
        new[i] += 13;
    }
  }

  strcat(new, a);
  strcpy(string, new);
}

void logged(char *command, char *msg, char *old, char *new)
{
  FILE *fptr;
  fptr = fopen("Wibu.log", "a");
  if (fptr == NULL)
  {
    printf("[Error] : [Gagal dalam membuka file]");
    exit(1);
  }
  fprintf(fptr, "%s\t%s\t%s\t-->\t%s\n", command, msg, old, new);
  fclose(fptr);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }
  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);
  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char *str = strstr(path, prefix);
  if (str != NULL)
  {
    str += strlen(prefix);
    char *temp = strchr(str, '/');
    if (temp != NULL)
    {
      temp += 1;
      enkripsi1(temp);
    }
  }
  int res;
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {
    if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
      continue;
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (str != NULL)
      enkripsi1(de->d_name);

    res = (filler(buf, de->d_name, &st, 0));
    if (res != 0)
      break;
  }

  closedir(dp);
  return 0;
}

static int xmp_rename(const char *source, const char *destination)
{
  int res;
  char fpath_source[1000];
  char fpath_destination[1000];
  char *p_fpath_source, *p_fpath_destination;

  if (strcmp(source, "/") == 0)
  {
    source = dirpath;
    sprintf(fpath_source, "%s", source);
  }
  else
    sprintf(fpath_source, "%s%s", dirpath, source);

  if (strcmp(source, "/") == 0)
    sprintf(fpath_destination, "%s", dirpath);
  else
    sprintf(fpath_destination, "%s%s", dirpath, destination);

  res = rename(fpath_source, fpath_destination);
  if (res == -1)
    return -errno;

  p_fpath_source = strrchr(fpath_source, '/');
  p_fpath_destination = strrchr(fpath_destination, '/');
  if (strstr(p_fpath_source, "Animeku_"))
    logged("RENAME", "terdecode", fpath_source, fpath_destination);
  if (strstr(p_fpath_destination, "Animeku_"))
    logged("RENAME", "terenkripsi", fpath_source, fpath_destination);

  return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
};
